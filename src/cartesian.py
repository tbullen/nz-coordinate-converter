"""
Contains functions for the conversion of coordinates between their geographical and cartesian representations.

This conversion follows the same process for the NZGD2000 and NZGD1949 datums, however different ellipsoid parameters
are used.

Equations taken from the Land Information New Zealand website.
"""

import math

# Ellipsoid parameters. NZGD1949 uses International 1924, NZGD2000 uses GRS80
# Semi-major axis (metres)
NZGD1949_a = 6378388.0
NZGD2000_a = 6378137.0

# Inverse flattening
NZGD1949_inv_f = 297.0
NZGD2000_inv_f = 298.257222101


def convert_geographic_to_cartesian(lat: float, lon: float, height: float, datum: str) -> (float, float, float):
    """
    Converts a set of geographic coordinates (latitude, longitude, elevation) into Cartesian coordinates (X, Y, Z)
    for the given datum.

    :param lat: The geographic latitude coordinate expressed in radians.
    :param lon: The geographic longitude coordinate expressed in radians.
    :param height: The geographic height coordinate expressed in metres.
    :param datum: The reference datum being used. Must be either 'NZGD1949' or 'NZGD2000'.
    :return: x (metres), y (metres), z (metres)
    """

    if datum == 'NZGD1949':
        a = NZGD1949_a
        f = 1 / NZGD1949_inv_f
    elif datum == 'NZGD2000':
        a = NZGD2000_a
        f = 1 / NZGD2000_inv_f
    else:
        raise Exception(f'Invalid datum specified: {datum}')

    e_sq = (2 * f) - (f ** 2)
    v = a / math.sqrt(1 - e_sq * (math.sin(lat) ** 2))

    x = (v + height) * math.cos(lat) * math.cos(lon)
    y = (v + height) * math.cos(lat) * math.sin(lon)
    z = (v * (1 - e_sq) + height) * math.sin(lat)

    return x, y, z


def convert_cartesian_to_geographic(x: float, y: float, z: float, datum: str) -> (float, float, float):
    """
    Converts a set of Cartesian coordinates (X, Y, Z) into geographic coordinates (latitude, longitude, height)
    for the given datum.

    :param x: X-axis cartesian coordinate expressed in metres.
    :param y: Y-axis cartesian coordinate expressed in metres.
    :param z: Z-axis cartesian coordinate expressed in metres.
    :param datum: The reference datum being used. Must be either 'NZGD1949' or 'NZGD2000'.
    :return: latitude, longitude, height. Expressed in radians (latitude, longitude), and ellipsoid height in metres
    (height)
    """

    if datum == 'NZGD1949':
        a = NZGD1949_a
        f = 1 / NZGD1949_inv_f
    elif datum == 'NZGD2000':
        a = NZGD2000_a
        f = 1 / NZGD2000_inv_f
    else:
        raise Exception(f'Invalid datum specified: {datum}')

    e_sq = (2 * f) - (f ** 2)
    p = math.sqrt(x ** 2 + y ** 2)
    r = math.sqrt(p ** 2 + z ** 2)
    micro = math.atan((z / p) * ((1 - f) + (e_sq * a / r)))

    lon = math.atan(y / x)
    if lon < 0:
        lon += math.pi

    num = (z * (1 - f)) + (e_sq * a * (math.sin(micro) ** 3))
    denom = (1 - f) * (p - (e_sq * a * (math.cos(micro) ** 3)))
    lat = math.atan(num / denom)

    height = p * math.cos(lat) + z * math.sin(lat) - a * math.sqrt(1 - e_sq * (math.sin(lat) ** 2))

    return lat, lon, height
