"""
This module contains functions to convert between a geographic coordinate in the NZGD1949 datum to a coordinate in the
New Zealand Map Grid (NZMG) projection.

These functions were translated from the C source code obtained from the Land Information New Zealand webpage.
"""

import math

RAD2DEG = 180.0 / math.pi                # degrees = RAD2DEG * radians

# The origin point of the NZMG coordinate system expressed in NZGD1949 coordinates
NZMG_ORIGIN_LAT = -41.0
NZMG_ORIGIN_LON = 173.0

# The false origin point of the NZMG
NZMG_FALSE_NORTHING = 6023150.0
NZMG_FALSE_EASTING = 2510000.0

# Ellipsoid parameters. NZGD1949 uses International 1924
NZGD1949_a = 6378388.0  # Semi-major axis (metres)
NZGD1949_inv_f = 297.0  # Inverse flattening

# Coefficients for conversion of NZMG coordinates to NZGD1949.
# From L+S technical note set 1973/32
cfi = [0.6399175073, -0.1358797613, 0.063294409, -0.02526853, 0.0117879, -0.0055161, 0.0026906, -0.001333, 0.00067, -0.00034]

cfl = [1.5627014243, 0.5185406398, -0.03333098, -0.1052906, -0.0368594, 0.007317, 0.01220, 0.00394, -0.0013]

cfb1 = [ 0.7557853228 +           0.0j,
          0.249204646 +   0.003371507j,
         -0.001541739 +   0.041058560j,
          -0.10162907 +    0.01727609j,
          -0.26623489 +   -0.36249218j,
           -0.6870983 +    -1.1651967j ]

cfb2 = [ 1.3231270439 +           0.0j,
         -0.577245789 +  -0.007809598j,
          0.508307513 +  -0.112208952j,
          -0.15094762 +    0.18200602j,
           1.01418179 +    1.64497696j,
            1.9660549 +     2.5127645j ]


def convert_NZMG_to_NZGD1949(easting: float, northing: float) -> (float, float):
    """
    Converts a pair of New Zealand Map Grid (NZMG) coordinates into a pair of NZGD1949 coordinates.

    :param easting: The NZMG easting value expressed in metres.
    :param northing: The NZMG northing value expressed in metres.
    :return: Latitude, longitude expressed in degrees.
    """

    z0 = complex((northing - NZMG_FALSE_NORTHING) / NZGD1949_a, (easting - NZMG_FALSE_EASTING) / NZGD1949_a)
    z1 = cfb2[5]

    for c in cfb2[:5][::-1]:
        z1 = (z1 * z0) + c
    z1 *= z0

    for _ in range(2):
        zn = cfb1[5] * 5
        zd = cfb1[5] * 6

        for i in list(range(1, 5))[::-1]:
            zn = (zn * z1) + (cfb1[i] * i)
            zd = (zd * z1) + (cfb1[i] * (i + 1))
        zn = z0 + (z1 * (zn * z1))
        zd = cfb1[0] + (zd * z1)
        z1 = zn / zd

    lon = NZMG_ORIGIN_LON + (z1.imag * RAD2DEG)

    tmp = z1.real
    accum = 0
    for c in cfl[::-1]:
        accum = (accum * tmp) + c
    accum *= tmp
    accum /= 3600e-5

    lat = NZMG_ORIGIN_LAT + accum
    return lat, lon


def convert_NZGD1949_to_NZMG(lat: float, lon: float) -> (float, float):
    """
    Converts a pair of NZGD1949 geographic coordinates into a pair of New Zealand Map Grid (NZMG) coordinates.

    :param lat: The NZGD1949 geographic latitude coordinate expressed in degrees.
    :param lon: The NZGD1949 geographic longitude coordinate expressed in degrees.
    :return: Easting, northing expressed in metres.
    """

    # Translate relative to NZMG origin point
    lat -= NZMG_ORIGIN_LAT
    lon -= NZMG_ORIGIN_LON

    lat = lat * 3600e-5
    lon = lon / RAD2DEG  # convert to radians

    accum = 0
    for c in cfi[::-1]:
        accum = accum * lat + c
    accum *= lat

    z0 = cfb1[5]
    z1 = complex(accum, lon)

    for c in cfb1[:5][::-1]:
        z0 = (z0 * z1) + c
    z0 *= z1

    northing = NZMG_FALSE_NORTHING + (z0.real * NZGD1949_a)
    easting = NZMG_FALSE_EASTING + (z0.imag * NZGD1949_a)

    return easting, northing
