"""
This module contains functions to convert between a geographic coordinate in the NZGD2000 datum to a coordinate in the
New Zealand Transverse Mercator (NZTM) projection.

These functions were translated from the C source code obtained from the Land Information New Zealand webpage.
"""

import math

RAD2DEG = 180.0 / math.pi                # degrees = RAD2DEG * radians

# The origin point of the NZTM coordinate system expressed in NZGD2000 coordinates
NZTM_ORIGIN_LON = 173.0  # a.k.a Central Meridian
NZTM_ORIGIN_LAT = 0.0

NZTM_SCALE_FACTOR = 0.9996

# The false origin point of the NZTM
NZTM_FALSE_NORTHING = 10000000.0
NZTM_FALSE_EASTING = 1600000.0

# Ellipsoid parameters. NZGD2000 uses GRS80
NZGD2000_a = 6378137.0          # Semi-major axis (metres)
NZGD2000_inv_f = 298.257222101  # Inverse flattening


def convert_NZTM_to_NZGD2000(easting: float, northing: float) -> (float, float):
    """
    Converts a pair of New Zealand Transverse Mercator (NZTM) coordinates into a pair of NZGD2000 coordinates.

    :param easting: The NZTM easting value expressed in metres.
    :param northing: The NZTM northing value expressed in metres.
    :return: Latitude, longitude expressed in degrees.
    """

    nzmt_proj = TMProjection(NZGD2000_a, NZGD2000_inv_f, (NZTM_ORIGIN_LON / RAD2DEG), NZTM_SCALE_FACTOR,
                             (NZTM_ORIGIN_LAT / RAD2DEG), NZTM_FALSE_EASTING, NZTM_FALSE_NORTHING, 1.0)

    lat, lon = nzmt_proj.convert_tm_to_geographic(easting, northing)

    lat *= RAD2DEG  # convert to degrees
    lon *= RAD2DEG

    return lat, lon


def convert_NZGD2000_to_NZTM(lat: float, lon: float) -> (float, float):
    """
    Converts a pair of NZGD2000 geographic coordinates into a pair of New Zealand Transverse Mercator (NZTM) coordinates.

    :param lat: The NZGD2000 geographic latitude coordinate expressed in degrees.
    :param lon: The NZGD2000 geographic longitude coordinate expressed in degrees.
    :return: Easting, northing expressed in metres.
    """

    lat /= RAD2DEG  # convert to radians
    lon /= RAD2DEG

    nzmt_proj = TMProjection(NZGD2000_a, NZGD2000_inv_f, (NZTM_ORIGIN_LON / RAD2DEG), NZTM_SCALE_FACTOR,
                             (NZTM_ORIGIN_LAT / RAD2DEG), NZTM_FALSE_EASTING, NZTM_FALSE_NORTHING, 1.0)

    return nzmt_proj.convert_geographic_to_tm(lat, lon)


class TMProjection:
    def __init__(self, a, rf, cm, sf, lto, fe, fn, utom):
        self.meridian = cm
        self.scale_factor = sf
        self.origin_lat = lto
        self.false_easting = fe
        self.false_northing = fn

        # Unit to metre conversion
        self.utom = utom

        # Ellipsoid parameters
        if rf != 0.0:
            self.f = 1.0 / rf
        else:
            self.f = 0.0

        self.a = a
        self.rf = rf
        self.e2 = (2 * self.f) - (self.f ** 2)
        self.ep2 = self.e2 / (1.0 - self.e2)
        self.om = self.meridian_arc(self.origin_lat)

    def meridian_arc(self, lat):
        """
        Returns the length of meridional arc (Helmert formula).

        Method based on Redfearn's formulation as expressed in GDA technical manual at
        http://www.anzlic.org.au/icsm/gdatm/index.html

        :return: the arc length in metres.
        """

        e2 = self.e2
        e4 = self.e2 ** 2
        e6 = e4 * self.e2

        A0 = 1 - (e2 / 4.0) - (3.0 * e4 / 64.0) - (5.0 * e6 / 256.0)
        A2 = (3.0 / 8.0) * (e2 + e4 / 4.0 + 15.0 * e6 / 128.0)
        A4 = (15.0 / 256.0) * (e4 + 3.0 * e6 / 4.0)
        A6 = 35.0 * e6 / 3072.0

        return self.a * (A0 * lat - A2 * math.sin(2 * lat) + A4 * math.sin(4 * lat) - A6 * math.sin(6 * lat))

    def foot_point_lat(self, m):
        """
        Calculates the foot point latitude from the meridional arc.

        Method based on Redfearn's formulation as expressed in GDA technical
        manual at http://www.anzlic.org.au/icsm/gdatm/index.html.

        :return: the foot point latitude (radians).
        """

        n = self.f / (2.0 - self.f)
        n2 = n ** 2
        n3 = n ** 3
        n4 = n ** 4

        g = self.a * (1.0 - n) * (1.0 - n2) * (1 + 9.0 * n2 / 4.0 + 225.0 * n4 / 64.0)
        sig = m / g

        phio = sig + (3.0 * n / 2.0 - 27.0 * n3 / 32.0) * math.sin(2.0 * sig) \
               + (21.0 * n2 / 16.0 - 55.0 * n4 / 32.0) * math.sin(4.0 * sig) \
               + (151.0 * n3 / 96.0) * math.sin(6.0 * sig) \
               + (1097.0 * n4 / 512.0) * math.sin(8.0 * sig)

        return phio

    def convert_geographic_to_tm(self, lat: float, lon: float) -> (float, float):
        """
        Converts a pair of geographic coordinates to the Transverse Mercator projection coordinates.
        """

        dlon = lon - self.meridian
        while dlon > math.pi:
            dlon -= (2 * math.pi)
        while dlon < -math.pi:
            dlon += (2 * math.pi)

        m = self.meridian_arc(lat)

        slt = math.sin(lat)

        eslt = (1.0 - self.e2 * slt * slt)
        eta = self.a / math.sqrt(eslt)
        rho = eta * (1.0 - self.e2) / eslt
        psi = eta / rho

        clt = math.cos(lat)
        w = dlon

        wc = clt * w
        wc2 = wc * wc

        t = slt / clt
        t2 = t ** 2
        t4 = t ** 4
        t6 = t ** 6

        trm1 = (psi - t2) / 6.0

        trm2 = (((4.0 * (1.0 - 6.0 * t2) * psi + (1.0 + 8.0 * t2)) * psi - 2.0 * t2) * psi + t4) / 120.0

        trm3 = (61 - 479.0 * t2 + 179.0 * t4 - t6) / 5040.0

        gce = (self.scale_factor * eta * dlon * clt) * (((trm3 * wc2 + trm2) * wc2 + trm1) * wc2 + 1.0)
        easting = gce / self.utom + self.false_easting

        trm1 = 1.0 / 2.0

        trm2 = ((4.0 * psi + 1) * psi - t2) / 24.0

        trm3 = ((((8.0 * (11.0 - 24.0 * t2) * psi - 28.0 * (1.0 - 6.0 * t2)) * psi + (1.0 - 32.0 * t2)) * psi - 2.0 * t2) * psi + t4) / 720.0

        trm4 = (1385.0 - 3111.0 * t2 + 543.0 * t4 - t6) / 40320.0

        gcn = (eta * t) * ((((trm4 * wc2 + trm3) * wc2 + trm2) * wc2 + trm1) * wc2)
        northing = (gcn + m - self.om) * self.scale_factor / self.utom + self.false_northing
        return easting, northing

    def convert_tm_to_geographic(self, easting: float, northing: float) -> (float, float):
        """
        Converts a pair of Transverse Mercator coordinates to geographic coordinates.
        """

        cn1 = (northing - self.false_northing) * self.utom / self.scale_factor + self.om
        fphi = self.foot_point_lat(cn1)
        slt = math.sin(fphi)
        clt = math.cos(fphi)

        eslt = (1.0 - self.e2 * slt * slt)
        eta = self.a / math.sqrt(eslt)
        rho = eta * (1.0 - self.e2) / eslt
        psi = eta / rho

        E = (easting - self.false_easting) * self.utom
        x = E / (eta * self.scale_factor)
        x2 = x ** 2

        t = slt / clt
        t2 = t ** 2
        t4 = t ** 4

        trm1 = 1.0 / 2.0

        trm2 = ((-4.0 * psi + 9.0 * (1 - t2)) * psi + 12.0 * t2) / 24.0

        trm3 = ((((8.0 * (11.0 - 24.0 * t2) * psi - 12.0 * (21.0 - 71.0 * t2)) * psi
                  + 15.0 * ((15.0 * t2 - 98.0) * t2 + 15)) * psi
                 + 180.0 * ((-3.0 * t2 + 5.0) * t2)) * psi + 360.0 * t4) / 720.0

        trm4 = (((1575.0 * t2 + 4095.0) * t2 + 3633.0) * t2 + 1385.0) / 40320.0

        lat = fphi + (t * x * E / (self.scale_factor * rho)) * (((trm4 * x2 - trm3) * x2 + trm2) * x2 - trm1)

        trm1 = 1.0

        trm2 = (psi + 2.0 * t2) / 6.0

        trm3 = (((-4.0 * (1.0 - 6.0 * t2) * psi
                  + (9.0 - 68.0 * t2)) * psi
                 + 72.0 * t2) * psi
                + 24.0 * t4) / 120.0

        trm4 = (((720.0 * t2 + 1320.0) * t2 + 662.0) * t2 + 61.0) / 5040.0

        lon = self.meridian - (x / clt) * (((trm4 * x2 - trm3) * x2 + trm2) * x2 - trm1)
        return lat, lon
