"""
This module contains a utility class to convert location coordinates between the New Zealand 1949 Geodetic Datum
(NZGD1949), the New Zealand 2000 Geodetic Datum (NZGD2000) and their map projections, the New Zealand Map Grid (NZMG)
and the New Zealand Transverse Mercator (NZTM) respectively.

The New Zealand Map Grid was the projection used to produce topographic maps of NZ until it was superseded in 2001 by
the NZTM2000 projection. It uses a complex-number polynomial expansion, which has the benefit of exhibiting minimum
scale distortion error but the drawback of being complicated to integrate within global systems or other coordinate
reference frames.

Coordinates in the NZGD2000 system are compatible with the WSG84 coordinate system which is used by GPS, unless
precision of less than one metre is required.

This module will hopefully ease the pain of developers who encounter old software, datasets, or legacy products that
still use the NZMG reference frame.
"""

__author__ = "Tim Bullen"
__copyright__ = ""
__license__ = ""
__version__ = "1.0"
__maintainer__ = "Tim Bullen"
__status__ = ""

import math
from .NZMG import convert_NZGD1949_to_NZMG, convert_NZMG_to_NZGD1949
from .NZTM import convert_NZGD2000_to_NZTM, convert_NZTM_to_NZGD2000
from .cartesian import convert_cartesian_to_geographic, convert_geographic_to_cartesian
from .datum import convert_NZGD1949_to_NZGD2000, convert_NZGD2000_to_NZGD1949

RAD2DEG = 180.0 / math.pi                # degrees = RAD2DEG * radians


class NZCoordinateConverter:
    """
    Converter class object that holds one geospatial point and provides methods of accessing the conversions of that
    point to other coordinate systems or mapping projections.
    """

    # 1949 NZ Geodetic Datum
    NZGD1949_lat = None
    NZGD1949_lon = None
    NZGD1949_height = None
    NZGD1949_x = None
    NZGD1949_y = None
    NZGD1949_z = None
    NZMG_easting = None
    NZMG_northing = None

    # 2000 NZ Geodetic Datum
    NZGD2000_lat = None
    NZGD2000_lon = None
    NZGD2000_height = None
    NZGD2000_x = None
    NZGD2000_y = None
    NZGD2000_z = None
    NZTM_easting = None
    NZTM_northing = None


    def __init__(self, datum: str, lat: float = None, lon: float = None, easting: float = None, northing: float = None,
                 height: float = 0.0):
        """
        Initialises an instance of the coordinate converter.

        To initialise, the user must specify the geodetic datum that is being used (either 'NZGD1949' or 'NZGD2000'),
        and one of the following:
            - A pair of geographic coordinates (latitude, longitude)
            - A pair of map projection coordinates (easting, northing)

        Once initialised, the class will compute the conversions of the provided point to all other reference systems
        and projections. They can be accessed by invoking the data retrieval methods of the class, eg.

            converter = NZCoordinateConverter('NZGD1949', easting=2939247.5, northing=6752871.3)
            latitude, longitude, _ = converter.get_NZGD2000_geographic()

        :param datum: The geodetic datum being used, 'NZGD1949' or 'NZGD2000'. For NZMG coordinates, use 'NZGD1949'.
                      For NZTM coordinates, use 'NZGD2000'.
        :param lat: Latitude coordinate expressed in degrees.
        :param lon: Longitude coordinate expressed in degrees.
        :param easting: Easting coordinate expressed in metres.
        :param northing: Northing coordinate expressed in metres.
        :param height: Height coordinate expressed in metres.
        """
        if datum == 'NZGD1949':
            self._init_NZGD1949_datum(lat, lon, easting, northing, height)

            nzgd2000_lat, nzgd2000_lon, nzgd2000_h = convert_NZGD1949_to_NZGD2000(self.NZGD1949_lat, self.NZGD1949_lon, self.NZGD1949_height)

            self._init_NZGD2000_datum(lat=nzgd2000_lat, lon=nzgd2000_lon, height=nzgd2000_h)

        elif datum == 'NZGD2000':
            self._init_NZGD2000_datum(lat, lon, easting, northing, height)

            nzgd1949_lat, nzgd1949_lon, nzgd1949_h = convert_NZGD2000_to_NZGD1949(self.NZGD2000_lat, self.NZGD2000_lon, self.NZGD2000_height)

            self._init_NZGD1949_datum(lat=nzgd1949_lat, lon=nzgd1949_lon, height=nzgd1949_h)

        else:
            raise Exception('Invalid geodetic datum specified. '
                            'Datum must be either "NZGD1949" or "NZGD2000".')

    # 1949 Geodetic Datum Retrieval Functions #

    def get_NZGD1949_geographic(self) -> (float, float, float):
        """
        Provides the geographic representation of the coordinate in the NZ 1949 Geodetic Datum reference system.

        :return: latitude (degrees), longitude (degrees), height (metres)
        """
        return self.NZGD1949_lat, self.NZGD1949_lon, self.NZGD1949_height

    def get_NZGD1949_cartesian(self) -> (float, float, float):
        """
        Provides the ECEF cartesian representation of the coordinate in the NZ 1949 Geodetic Datum reference system.

        :return: x (metres), y (metres), z (metres)
        """
        return self.NZGD1949_x, self.NZGD1949_y, self.NZGD1949_z

    def get_NZMG(self) -> (float, float, float):
        """
        Provides the location of the coordinate projected onto the New Zealand Map Grid projection.
        This projection is based on the NZ 1949 Geodetic Datum reference system.

        :return: easting (metres), northing (metres), height (metres)
        """
        return self.NZMG_easting, self.NZMG_northing, self.NZGD1949_height

    # 2000 Geodetic Datum Retrieval Functions #

    def get_NZGD2000_geographic(self) -> (float, float, float):
        """
        Provides the geographic representation of the coordinate in the NZ 2000 Geodetic Datum reference system.

        :return: latitude (degrees), longitude (degrees), height (metres)
        """
        return self.NZGD2000_lat, self.NZGD2000_lon, self.NZGD2000_height

    def get_NZGD2000_cartesian(self) -> (float, float, float):
        """
        Provides the ECEF cartesian representation of the coordinate in the NZ 2000 Geodetic Datum reference system.

        :return: x (metres), y (metres), z (metres)
        """
        return self.NZGD2000_x, self.NZGD2000_y, self.NZGD2000_z

    def get_NZTM(self) -> (float, float, float):
        """
        Provides the location of the coordinate projected onto the New Zealand Transverse Mercator projection.
        This projection is based on the NZ 2000 Geodetic Datum reference system.

        :return: easting (metres), northing (metres), height (metres)
        """
        return self.NZTM_easting, self.NZTM_northing, self.NZGD2000_height

    # Initialisation functions #

    def _init_NZGD1949_datum(self, lat: float = None, lon: float = None, easting: float = None, northing: float = None,
                             height: float = None):
        """Initialises the data for the NZ 1949 Geodetic Datum."""

        self.NZGD1949_height = height

        if lat is not None and lon is not None:
            self.NZGD1949_lat = lat
            self.NZGD1949_lon = lon
            self.NZMG_easting, self.NZMG_northing = convert_NZGD1949_to_NZMG(self.NZGD1949_lat, self.NZGD1949_lon)
        elif easting is not None and northing is not None:
            self.NZMG_easting = easting
            self.NZMG_northing = northing
            self.NZGD1949_lat, self.NZGD1949_lon = convert_NZMG_to_NZGD1949(self.NZMG_easting, self.NZMG_northing)
        else:
            raise Exception('Unable to instantiate class without a complete location coordinate.')

        # convert to radians and get cartesian coords
        lat_rad = self.NZGD1949_lat / RAD2DEG
        lon_rad = self.NZGD1949_lon / RAD2DEG
        self.NZGD1949_x, self.NZGD1949_y, self.NZGD1949_z = convert_geographic_to_cartesian(
            lat_rad, lon_rad, self.NZGD1949_height, 'NZGD1949'
        )
        return

    def _init_NZGD2000_datum(self, lat: float = None, lon: float = None, easting: float = None, northing: float = None,
                             height: float = None):
        """Initialises the data for the NZ 2000 Geodetic Datum."""

        self.NZGD2000_height = height

        if lat is not None and lon is not None:
            self.NZGD2000_lat = lat
            self.NZGD2000_lon = lon
            self.NZTM_easting, self.NZTM_northing = convert_NZGD2000_to_NZTM(self.NZGD2000_lat, self.NZGD2000_lon)
        elif easting is not None and northing is not None:
            self.NZTM_easting = easting
            self.NZTM_northing = northing
            self.NZGD2000_lat, self.NZGD2000_lon = convert_NZTM_to_NZGD2000(self.NZTM_easting, self.NZTM_northing)
        else:
            raise Exception('Unable to instantiate class without a complete location coordinate.')

        lat_rad = self.NZGD2000_lat / RAD2DEG
        lon_rad = self.NZGD2000_lon / RAD2DEG
        self.NZGD2000_x, self.NZGD2000_y, self.NZGD2000_z = convert_geographic_to_cartesian(
            lat_rad, lon_rad, self.NZGD2000_height, 'NZGD2000'
        )
        return

