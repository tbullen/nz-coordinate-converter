"""
Functions for converting between the NZGD1949 and NZGD2000 datums.

There are several methods of achieving this, each with different accuracies. These are outlined on the LINZ website.
This module uses the 7-parameter similarity transform, which achieves a nominal accuracy of at least 4 metres.

Equations taken from the Land Information New Zealand website.
"""

import math
import numpy as np
from .cartesian import convert_geographic_to_cartesian, convert_cartesian_to_geographic

RAD2DEG = 180.0 / math.pi                # degrees = RAD2DEG * radians
ARCSECS2RAD = math.pi / (180 * 3600)     # radians = ARCSECS2RAD * arcseconds

# Coefficients for the conversion of NZGD1949 coordinates to NZGD2000 coordinates
# From the Standard for New Zealand Geodetic Datum 2000 - LINZS25000
# Using the seven parameter transformation method. Accuracy = ~4 m
# For the inverse method, i.e (NZGD2000 -> NZGD1949), the sign of the coefficients is reversed
Tx = 59.47  # Translation (metres)
Ty = -5.04
Tz = 187.44  # Rotation (arcseconds)
Rx = -0.470
Ry = 0.100
Rz = -1.024
s = -4.5993  # Scale change (parts per million)


def convert_NZGD1949_to_NZGD2000(lat: float, lon: float, height: float) -> (float, float, float):
    """
    Converts a geographic representation of a coordinate point in the NZGD1949 datum to the NZGD2000 datum.

    :param lat: The NZGD1949 geographic latitude coordinate expressed in degrees.
    :param lon: The NZGD1949 geographic longitude coordinate expressed in degrees.
    :return: latitude, longitude of the same point expressed in the NZGD2000 datum.
    """
    # Convert to radians
    lat /= RAD2DEG
    lon /= RAD2DEG

    # Convert the geographic coords to cartesian
    x, y, z = convert_geographic_to_cartesian(lat, lon, 0.0, 'NZGD1949')

    # Convert the rotations from arcseconds to radians
    rx = Rx * ARCSECS2RAD
    ry = Ry * ARCSECS2RAD
    rz = Rz * ARCSECS2RAD

    # Create vectors and matrices for similarity transform
    loc_vector = np.array([x, y, z])
    rot_matrix = np.array([[1, rz, -1 * ry],
                           [-1 * rz, 1, rx],
                           [ry, -1 * rx, 1]])
    scalar = 1 + (s * 1e-6)
    translation_vector = np.array([Tx, Ty, Tz])

    # Similarity transform
    loc_vector = perform_similarity_transformation(loc_vector, translation_vector, rot_matrix, scalar)

    # convert back to geographic coords in new datum
    lat, lon, _ = convert_cartesian_to_geographic(loc_vector[0], loc_vector[1], loc_vector[2], 'NZGD2000')

    # convert to degrees
    lat *= RAD2DEG
    lon *= RAD2DEG

    return lat, lon, height


def convert_NZGD2000_to_NZGD1949(lat: float, lon: float, height: float) -> (float, float, float):
    """
    Converts a geographic representation of a coordinate point in the NZGD2000 datum to the NZGD1949 datum.

    :param lat: The NZGD2000 geographic latitude coordinate expressed in degrees.
    :param lon: The NZGD2000 geographic longitude coordinate expressed in degrees.
    :return: latitude, longitude of the same point expressed in the NZGD1949 datum.
    """
    # Convert to radians
    lat /= RAD2DEG
    lon /= RAD2DEG

    # Convert the geographic coords to cartesian
    x, y, z = convert_geographic_to_cartesian(lat, lon, 0.0, 'NZGD2000')

    # Convert the rotations from arcseconds to radians
    rx = Rx * ARCSECS2RAD
    ry = Ry * ARCSECS2RAD
    rz = Rz * ARCSECS2RAD

    # Create vectors and matrices for similarity transform. Negative coefficients for the reverse transform.
    loc_vector = np.array([x, y, z])
    rot_matrix = np.array([[1, -1 * rz, ry],
                           [rz, 1, -1 * rx],
                           [-1 * ry, rx, 1]])
    scalar = 1 + (-1 * s * 1e-6)
    translation_vector = np.array([Tx, Ty, Tz])
    translation_vector *= -1

    # Similarity transform
    loc_vector = perform_similarity_transformation(loc_vector, translation_vector, rot_matrix, scalar)

    # convert back to geographic coords in new datum
    lat, lon, _ = convert_cartesian_to_geographic(loc_vector[0], loc_vector[1], loc_vector[2], 'NZGD1949')

    # convert to degrees
    lat *= RAD2DEG
    lon *= RAD2DEG

    return lat, lon, height


def perform_similarity_transformation(loc_vec: np.array, translation_vec: np.array, rot_matrix: np.array,
                                      scalar: float) -> np.array:
    """
    Performs the similarity transform on the location vector, given the translation, rotation and scalar transforms.

    :param loc_vec: The input cartesian coordinates vector (x, y, z). Expressed in metres.
    :param translation_vec: 3x1 vector describing the translation transformation.
    :param rot_matrix: 3x3 vector describing the rotation transformation.
    :param scalar: Value describing the scalar transformation.
    :return: Output cartesian coordinates vector (x, y, z). Expressed in metres.
    """
    # perform rotation
    loc_vec = np.matmul(rot_matrix, loc_vec)

    # scale shift
    loc_vec *= scalar

    # translation
    loc_vec += translation_vec
    return loc_vec
