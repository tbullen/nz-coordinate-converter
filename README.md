# NZ Coordinate Converter

A Python utility class to convert location coordinates between the New Zealand 1949 Geodetic Datum (NZGD1949), the New Zealand 2000 Geodetic Datum (NZGD2000) and their map projections, the New Zealand Map Grid (NZMG)
and the New Zealand Transverse Mercator (NZTM).

## What are they?

The 1949 Geodetic Datum was the official geodetic datum of New Zealand until it was superseded in 1998 by NZGD2000.

The New Zealand Map Grid was the projection based on the 1949 datum used to produce topographic maps of NZ until it was superseded in 2001 by the NZTM2000 projection, based on the new 2000 datum.

According to [Land Information New Zealand](https://www.linz.govt.nz/guidance/geodetic-system), the 2000 NZ Geodetic Datum is identical to the WSG84 coordinate system (as used by GPS) at one metre above sea level. Unless accuracies greater than one metre are required, it is safe to use NZGD2000 coordinates in WSG84 systems.

## Why this exists

While the 1949 NZ datum and its map projection are deprecated, researchers and engineers will still regularly come across old datasets, legacy prducts, or literature references that use these coordinate systems. A method of programmatically being able to update these coordinates to the current reference systems will mean better retention of crucial data.

There are several existing conversion libraries out there in different programming languages that offer conversions between one or two coordinate systems, but none that offered full conversion between the main forms of coordinate representation and geodetic datums all in one place.

The underlying conversion functions to move between the coordinate systems are untrivial and can involve complex-number polynomial expansion, so the use of a conversion library can greatly improve reliability of software and user productivity.
 
This module will hopefully ease the pain of developers who encounter old software, datasets, or legacy products that still use the NZMG reference frame.

## Usage

First, import the `NZCoordinateConverter` class from the `NZ_coordinate_converter.py` file.

	>>> from NZ_coordinate_converter import NZCoordinateConverter

Initialise an object of the `NZCoordinateConverter` class. To initialise, the user must specify the geodetic datum that is being used (either 'NZGD1949' or 'NZGD2000'), and one of the following:
 - A pair of geographic coordinates (latitude, longitude)
 - A pair of map projection coordinates (easting, northing)

Once initialised, the class will compute the conversions of the provided point to all other reference systems and projections. They can be accessed by invoking the data retrieval methods of the class. 

The following converts a coordinate in NZMG coordinates to geographic coordinates in the NZGD2000 system:

	>>> converter = NZCoordinateConverter('NZGD1949', easting=2939247.5, northing=6752871.3)
	>>> # Returns latitude, longitude, height
	>>> converter.get_NZGD2000_geographic()
	(-34.325265329728666, 177.65074690413948, 0.0)

Or to retrieve the NZGD1949 coordinates:

	>>> converter.get_NZGD1949_geographic()
	(-34.32710058032676, 177.65052347813625, 0.0)


## Contributing

If you have an improvement or have found a bug please feel free to open an issue or add the change and create a pull request. If you file a bug please make sure to include as much information about your environment as possible to help reproduce the issue.
