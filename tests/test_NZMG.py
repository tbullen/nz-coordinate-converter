from src.NZMG import convert_NZMG_to_NZGD1949, convert_NZGD1949_to_NZMG


def test_conversion_round_trip_a():
    test_lat, test_lon = -40.512409, 172.723106

    easting, northing = convert_NZGD1949_to_NZMG(test_lat, test_lon)
    print(f'{easting} E, {northing} N')

    assert round(easting) == 2486533
    assert round(northing) == 6077264

    lat, lon = convert_NZMG_to_NZGD1949(easting, northing)
    print(f'{lat}, {lon}')

    assert (lat - test_lat) < 1e-5
    assert (lon - test_lon) < 1e-5
    return


def test_conversion_round_trip_b():
    test_lat, test_lon = -34.444066, 172.739194

    easting, northing = convert_NZGD1949_to_NZMG(test_lat, test_lon)
    print(f'{easting} E, {northing} N')

    assert round(easting) == 2487101
    assert round(northing) == 6751050

    lat, lon = convert_NZMG_to_NZGD1949(easting, northing)
    print(f'{lat}, {lon}')

    assert (lat - test_lat) < 1e-5
    assert (lon - test_lon) < 1e-5
    return
