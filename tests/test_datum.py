"""
Tests the conversion of geographic coordinates between the NZGD1949 datum and the NZGD2000 datum.

Examples taken from Land Information New Zealand webpage.
"""

import pytest

from src.datum import convert_NZGD1949_to_NZGD2000, convert_NZGD2000_to_NZGD1949


def test_convert_nzgd1949_nzgd2000_round_trip_a():
    nzgd1949_lat, nzgd1949_lon, nzgd1949_height = -36.500000, 175.000000, 35.0

    nzgd2000_lat, nzgd2000_lon, nzgd2000_height = convert_NZGD1949_to_NZGD2000(nzgd1949_lat, nzgd1949_lon, nzgd1949_height)

    assert nzgd2000_lat == pytest.approx(-36.498193)
    assert nzgd2000_lon == pytest.approx(175.000185)
    assert nzgd2000_height == pytest.approx(35.0)

    print(f'NZGD2000 Expected: -36.498193, 175.000185')
    print(f'NZGD2000 Calc:     {nzgd2000_lat:.6f}, {nzgd2000_lon:.6f}')
    test_nzgd1949_lat, test_nzgd1949_lon, test_nzgd1949_height = convert_NZGD2000_to_NZGD1949(
        nzgd2000_lat, nzgd2000_lon, nzgd2000_height)

    assert test_nzgd1949_lat == pytest.approx(nzgd1949_lat)
    assert test_nzgd1949_lon == pytest.approx(nzgd1949_lon)
    assert test_nzgd1949_height == pytest.approx(nzgd1949_height)


def test_convert_nzgd1949_nzgd2000_round_trip_b():
    nzgd1949_lat, nzgd1949_lon, nzgd1949_height = -41.250000, 174.750000, 128.9

    nzgd2000_lat, nzgd2000_lon, nzgd2000_height = convert_NZGD1949_to_NZGD2000(nzgd1949_lat, nzgd1949_lon, nzgd1949_height)

    assert nzgd2000_lat == pytest.approx(-41.248287)
    assert nzgd2000_lon == pytest.approx(174.750164)
    assert nzgd2000_height == pytest.approx(128.9)

    print(f'NZGD2000 Expected: -41.248287, 174.750164')
    print(f'NZGD2000 Calc:     {nzgd2000_lat:.6f}, {nzgd2000_lon:.6f}')
    test_nzgd1949_lat, test_nzgd1949_lon, test_nzgd1949_height = convert_NZGD2000_to_NZGD1949(
        nzgd2000_lat, nzgd2000_lon, nzgd2000_height)

    assert test_nzgd1949_lat == pytest.approx(nzgd1949_lat)
    assert test_nzgd1949_lon == pytest.approx(nzgd1949_lon)
    assert test_nzgd1949_height == pytest.approx(nzgd1949_height)


def test_convert_nzgd1949_nzgd2000_round_trip_c():
    nzgd1949_lat, nzgd1949_lon, nzgd1949_height = -45.000000, 170.500000, -20.0

    nzgd2000_lat, nzgd2000_lon, nzgd2000_height = convert_NZGD1949_to_NZGD2000(nzgd1949_lat, nzgd1949_lon, nzgd1949_height)

    assert nzgd2000_lat == pytest.approx(-44.998370)
    assert nzgd2000_lon == pytest.approx(170.500091)
    assert nzgd2000_height == pytest.approx(-20.0)

    print(f'NZGD2000 Expected: -44.998370, 170.500091')
    print(f'NZGD2000 Calc:     {nzgd2000_lat:.6f}, {nzgd2000_lon:.6f}')
    test_nzgd1949_lat, test_nzgd1949_lon, test_nzgd1949_height = convert_NZGD2000_to_NZGD1949(
        nzgd2000_lat, nzgd2000_lon, nzgd2000_height)

    assert test_nzgd1949_lat == pytest.approx(nzgd1949_lat)
    assert test_nzgd1949_lon == pytest.approx(nzgd1949_lon)
    assert test_nzgd1949_height == pytest.approx(nzgd1949_height)

