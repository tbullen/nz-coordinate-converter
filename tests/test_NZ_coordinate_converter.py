import pytest

from src.NZ_coordinate_converter import NZCoordinateConverter


def test_nzmg_to_nzgd2000_a():
    """Example created using the LINZ online converter tool."""

    nzmg_easting = 2939247.5
    nzmg_northing = 6752871.3
    nzmg_height = 18.27

    converter = NZCoordinateConverter('NZGD1949', easting=nzmg_easting, northing=nzmg_northing, height=nzmg_height)

    lat, lon, height = converter.get_NZGD1949_geographic()

    assert lat == pytest.approx(-34.32710058)
    assert lon == pytest.approx(177.65052348)
    assert height == pytest.approx(18.27)

    lat, lon, height = converter.get_NZGD2000_geographic()

    assert lat == pytest.approx(-34.32525405)
    assert lon == pytest.approx(177.65067310)
    assert height == pytest.approx(18.27)

    x, y, z = converter.get_NZGD2000_cartesian()

    assert x == pytest.approx(-5268614.694, 0.1)
    assert y == pytest.approx(216152.740, 0.1)
    assert z == pytest.approx(-3576332.926, 0.1)


def test_nzmg_to_nzgd2000_b():
    """Example created using the LINZ online converter tool."""

    nzmg_easting = 2855246
    nzmg_northing = 6314186
    nzmg_height = 128

    converter = NZCoordinateConverter('NZGD1949', easting=nzmg_easting, northing=nzmg_northing, height=nzmg_height)

    lat, lon, height = converter.get_NZGD2000_geographic()

    assert lat == pytest.approx(-38.30725371)
    assert lon == pytest.approx(176.94746315)
    assert height == pytest.approx(128.0)

    x, y, z = converter.get_NZGD2000_cartesian()

    assert x == pytest.approx(-5004370.797, 0.1)
    assert y == pytest.approx(266869.499, 0.1)
    assert z == pytest.approx(-3932358.379, 0.1)
