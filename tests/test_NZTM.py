from src.NZTM import convert_NZTM_to_NZGD2000, convert_NZGD2000_to_NZTM


def test_conversion_round_trip_a():
    test_lat, test_lon = -40.512409, 172.723106

    easting, northing = convert_NZGD2000_to_NZTM(test_lat, test_lon)
    print(f'{easting} E, {northing} N')

    assert round(easting) == 1576542
    assert round(northing) == 5515331

    lat, lon = convert_NZTM_to_NZGD2000(easting, northing)
    print(f'{lat}, {lon}')

    assert (lat - test_lat) < 1e-5
    assert (lon - test_lon) < 1e-5
    return


def test_conversion_round_trip_b():
    test_lat, test_lon = -34.444066, 172.739194

    easting, northing = convert_NZGD2000_to_NZTM(test_lat, test_lon)
    print(f'{easting} E, {northing} N')

    assert round(easting) == 1576041
    assert round(northing) == 6188574

    lat, lon = convert_NZTM_to_NZGD2000(easting, northing)
    print(f'{lat}, {lon}')

    assert (lat - test_lat) < 1e-5
    assert (lon - test_lon) < 1e-5
    return
